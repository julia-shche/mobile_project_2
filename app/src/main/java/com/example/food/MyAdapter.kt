package com.example.food

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.food.data.Manufacturer
import kotlinx.android.synthetic.main.item_list.view.*

class MyAdapter(
    private var list: List<Manufacturer>,
    private val itemClickListener: ItemClickListener
) : RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.item_list, parent, false)

        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val manufacturer = list[position]
        holder.bind(manufacturer, itemClickListener)
    }

    override fun getItemCount(): Int = list.size

    fun setMovieListItems(list: List<Manufacturer>){
        this.list = list
        notifyDataSetChanged()

    }



    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(manufacturer: Manufacturer, itemClickListener: ItemClickListener) {
            itemView.title.text = manufacturer.publicName
            itemView.setOnClickListener { itemClickListener.onItemClick(manufacturer) }
        }
    }

    interface ItemClickListener {
        fun onItemClick(manufacturer: Manufacturer)
    }
}
