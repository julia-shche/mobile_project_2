package com.example.food

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.food.data.Manufacturer
import kotlinx.android.synthetic.main.activity_main2.*
import androidx.recyclerview.widget.LinearLayoutManager


class MainActivity2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)


        val intent = getIntent()
        val inf_manufacture = intent.getSerializableExtra("manufacturer") as Manufacturer

        val inf_specialization = inf_manufacture.specialization


        name.text = inf_manufacture.publicName
        country.text = inf_manufacture.country

        val adapter2 = MyAdapter2(this, inf_specialization)



        list2.apply {
            layoutManager = LinearLayoutManager(this@MainActivity2).apply {
                orientation = LinearLayoutManager.VERTICAL
            }
            this.adapter = adapter2
        }


    }


    companion object {
        const val KEY_MANUFACTURER = "manufacturer"
    }
}

