package com.example.food

import com.example.food.data.ApiReponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import com.google.gson.GsonBuilder
import com.google.gson.Gson



interface ApiInterface{
    @GET("getallmanufacturers?format=json")
    fun getInf() : Call<ApiReponse>

    companion object{
        var BASE_URL = "https://vpic.nhtsa.dot.gov/api/vehicles/"

        fun create() : ApiInterface {
            val gson = GsonBuilder()
                .setLenient()
                .create()

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson   ))
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }



}