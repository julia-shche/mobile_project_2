package com.example.food

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.food.data.ApiReponse
import com.example.food.data.Manufacturer
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback

class MainActivity : AppCompatActivity() {

    //    val json = "{\n" +
//            "  \"Count\": 95,\n" +
//            "  \"Message\": \"Response returned successfully\",\n" +
//            "  \"SearchCriteria\": null,\n" +
//            "  \"Results\": [\n" +
//            "    {\n" +
//            "      \"Country\": \"UNITED STATES (USA)\",\n" +
//            "      \"Mfr_CommonName\": \"Tesla\",\n" +
//            "      \"Mfr_ID\": 955,\n" +
//            "      \"Mfr_Name\": \"TESLA, INC.\",\n" +
//            "      \"VehicleTypes\": [\n" +
//            "        {\n" +
//            "          \"IsPrimary\": true,\n" +
//            "          \"Name\": \"Passenger Car\"\n" +
//            "        },\n" +
//            "        {\n" +
//            "          \"IsPrimary\": false,\n" +
//            "          \"Name\": \"Multipurpose Passenger Vehicle (MPV)\"\n" +
//            "        }\n" +
//            "      ]\n" +
//            "    },\n" +
//            "    {\n" +
//            "      \"Country\": \"UNITED KINGDOM (UK)\",\n" +
//            "      \"Mfr_CommonName\": \"Aston Martin\",\n" +
//            "      \"Mfr_ID\": 956,\n" +
//            "      \"Mfr_Name\": \"ASTON MARTIN LAGONDA LIMITED\",\n" +
//            "      \"VehicleTypes\": []\n" +
//            "    }\n" +
//            "  ]\n" +
//            "}"
    val list_manufacture: ArrayList<Manufacturer> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = MyAdapter(list_manufacture, object : MyAdapter.ItemClickListener {
            override fun onItemClick(manufacturer: Manufacturer) {
                val intent = Intent(this@MainActivity, MainActivity2::class.java)
                intent.putExtra(MainActivity2.KEY_MANUFACTURER, manufacturer)

                startActivity(intent)
            }
        })




        list.apply {
            layoutManager = LinearLayoutManager(this@MainActivity).apply {
                orientation = LinearLayoutManager.VERTICAL
            }
            this.adapter = adapter
        }

//        val result = Gson().fromJson(json, ApiReponse::class.java)

        val apiInterface = ApiInterface.create().getInf()



        apiInterface.enqueue(object : retrofit2.Callback<ApiReponse> {
            override fun onFailure(call: Call<ApiReponse>, t: Throwable) {
                val a = 1
            }

            override fun onResponse(call: Call<ApiReponse>, response: Response<ApiReponse>) {
                for (res in response.body()!!.results) {
                    list_manufacture.add(res)
                }
                adapter.setMovieListItems(list_manufacture)
            }

        })


    }
}
