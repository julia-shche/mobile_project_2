package com.example.food

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.food.data.Specialization
import kotlinx.android.synthetic.main.item_list2.view.*

class MyAdapter2(private val mainActivity2: MainActivity2,     private val list2: List<Specialization>
) : RecyclerView.Adapter<MyAdapter2.MyViewHolder2>() {






    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder2 {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.item_list2, parent, false)

        return MyViewHolder2(mainActivity2, view)
    }

    override fun onBindViewHolder(holder: MyViewHolder2, position: Int) {
        val manufacturer = list2[position]
        holder.bind(manufacturer)
//        holder.bind(manufacturer, itemClickListener )
    }

    override fun getItemCount(): Int = list2.size










    class MyViewHolder2(val mainActivity2: MainActivity2, view: View) : RecyclerView.ViewHolder(view) {
        fun bind(specialization: Specialization) {
            itemView.title2.text = specialization.name




          if (specialization.IsPrimary == true) {
              itemView.image_special.setImageDrawable(mainActivity2.getResources().getDrawable(R.drawable.ic_check_black_24dp))
          }
          else if (specialization.IsPrimary == false){
              itemView.image_special.setImageDrawable(mainActivity2.getResources().getDrawable(R.drawable.ic_close_black_24dp))
          }

        }
    }

    interface ItemClickListener {
        fun onItemClick(manufacturer: Specialization)
    }
}