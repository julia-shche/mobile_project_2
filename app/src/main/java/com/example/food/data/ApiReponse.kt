package com.example.food.data

import com.google.gson.annotations.SerializedName

data class ApiReponse(
    @SerializedName("Results")
    val results: List<Manufacturer>
)