package com.example.food.data

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Manufacturer(
    @SerializedName("Mfr_Name")
    val publicName: String,

    @SerializedName("Country")
    val country: String,

    @SerializedName("VehicleTypes")
    val specialization: List<Specialization>
) : Serializable