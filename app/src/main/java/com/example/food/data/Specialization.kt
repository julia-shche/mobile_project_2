package com.example.food.data

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Specialization(
    @SerializedName("Name")
    val name: String,
    val IsPrimary: Boolean
) : Serializable